# File Cipher 

## Project description
A simple C programming beginner project. User can create, edit and delete files. Futhermore, user can encrypt and decrypt the created file. The program uses ceaser cipher to encrypt and decrypt the file. 

Simple cipher algorithm is used for now. Later advanced cipher algorithm will be added.

## Project Path

-   **Choose a Cryptographic Algorithm** 
    -   Decide which cryptographic algorithm you want to implement.

-   __Set Up File Input/Output__
    -   Write functions to read data from a file and write data to a file.

-   __Implement Encryption and Decryption Functions__
    -   Write functions to perform encryption and decryption using your chosen algorithm.

-   __Key Generation__ *in future*
    -   Implement a method for generating encryption keys. You could generate a random key or prompt the user to input a key.

-   __User Interface__
    -   Create a simple command-line interface for users to specify the input file, output file, and encryption key.

## File Management
-   .vscode
-   main
    -   file.h
    -   file.c
    -   file_operation.c
    -   file_management.c
-   readme.md

## Useful c programming stuff

__fopen__
-   filename: Name of the file to be opened
-   mode: Mode in which the file should be opened (e.g., read, write, append, etc.).

Common File Modes:
-   "r": Opens a file for reading. The file must exist.
-   "w": Opens a file for writing. If the file exists, its contents are overwritten. If the file does not exist, it is created.    
-   "a": Opens a file for appending. New data is added to the end of the file. If the file does not exist, it is created.
-   "r+": Opens a file for both reading and writing. The file must exist.
-   "w+": Opens a file for reading and writing. If the file exists, its contents are overwritten. If the file does not exist, it is created.
-   "a+": Opens a file for reading and appending. New data is added to the end of the file. If the file does not exist, it is created.