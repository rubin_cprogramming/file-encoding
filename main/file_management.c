/*
    file to manage files like create delete or edit
*/

#include "file.h"
#include <stdio.h>
#include <stdlib.h>

void createFile(char *fileName){
    FILE *file = fopen(fileName, "w");
    if (file == NULL){
        perror("Error creating file");
        return;
    }
    fclose(file);
    printf("File created successfully.\n");
    
}

void editFile(char *fileName){
    char data[1000];
    FILE *file = fopen(fileName, "a");
    if (file == NULL) {
        perror("Error opening file for editing");
        return;
    }

    int c;
    while ((c = getchar()) != '\n' && c != EOF);

    printf("Enter data to append to the file (max 1000 characters):\n");
    fgets(data, sizeof(data), stdin);
    fprintf(file, "%s\n", data);
    fclose(file);
    printf("Data appended to the file successfully.\n");
}

void deleteFile(char *fileName){
   if (remove(fileName) != 0) {
        perror("Error deleting file");
        return;
    }
    printf("File deleted successfully.\n");
}
