/*
    Header file for file encrypt and decrypt project
*/

#ifndef FILE_H
#define FILE_H

#define SHIFT 3
#define MAX_FILE_LENGTH 100

#include <stdio.h>

void encryptFile(FILE *inputFile, char *fileName);
void decryptFile(FILE *inputFile, char *fileName);

void createFile(char *fileName);
void editFile(char *fileName);
void deleteFile(char *fileName);

#endif /* FILE_H*/