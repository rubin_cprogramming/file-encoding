/*
    File to encrypt or decrypt files
    uses ceaser method
*/

#include "file.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void encryptFile(FILE *inputFile, char *fileName){

    char ch;
    FILE *outputFile;

    // Dynamically allocate memory for output file name
    char *outputFileName = malloc(strlen(fileName) + 7); 
    if (outputFileName == NULL) {
        perror("Memory allocation failed");
        fclose(inputFile);
        return;
    }

    // Construct output file name
    sprintf(outputFileName, "%s_e.txt", fileName);

    outputFile = fopen(outputFileName, "w");
    if (outputFile == NULL) {
        perror("Error opening output file");
        free(outputFileName); // Free memory before returning
        fclose(inputFile);
        return;
    }

    while ((ch = fgetc(inputFile)) != EOF){
        if (ch >= 'A' && ch <= 'Z'){
            ch = 'A' + (ch - 'A' + SHIFT) % 26;
        }
        else if (ch >= 'a' && ch <= 'z'){
            ch = 'a' + (ch - 'a' + SHIFT) % 26; 
        }
        
        fputc(ch, outputFile);
    }

    fclose(outputFile);
    free(outputFileName);
}

void decryptFile(FILE *inputFile, char *fileName){
    char ch;
    FILE *outputFile;

    // Dynamically allocate memory for output file name
    char *outputFileName = malloc(strlen(fileName) + 8); // "_de.txt" + null terminator
    if (outputFileName == NULL) {
        perror("Memory allocation failed");
        fclose(inputFile);
        return;
    }

    // Construct output file name
    sprintf(outputFileName, "%s_de.txt", fileName);

    outputFile = fopen(outputFileName, "w");
    if (outputFile == NULL) {
        perror("Error opening output file");
        free(outputFileName); // Free memory before returning
        fclose(inputFile);
        return;
    }

    while ((ch = fgetc(inputFile)) != EOF) {
       
        if (ch >= 'A' && ch <= 'Z'){
            ch = 'A' + (ch - 'A' - SHIFT + 26) % 26;
        }
        else if (ch >= 'a' && ch <= 'z'){
            ch = 'a' + (ch - 'a' - SHIFT + 26) % 26;
        } 
        fputc(ch, outputFile);
    }

    fclose(outputFile);
    free(outputFileName);

}