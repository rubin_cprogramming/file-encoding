/*
    Main file of file encrytion and decryption project
*/

#include "file.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(){

    FILE *inputFile;
    int choice = 0;
    int c;

    char *fileName = malloc(MAX_FILE_LENGTH * sizeof(char));
    if (fileName == NULL) {
        printf("Memory allocation failed. Exiting program.\n");
    return 1;
    }

    char *inputFileName = malloc(MAX_FILE_LENGTH * sizeof(char));
    if (inputFileName == NULL) {
        printf("Memory allocation failed. Exiting program.\n");
        free(fileName);
    return 1;
    }


    do{
        printf("1. Encrypt file.\n");
        printf("2. Decrypt file.\n");
        printf("3. File Management.\n");
        printf("4. Exit the program.\n");
        printf("Your choice (1-4): ");
        
        // Input and error handling
        if (scanf("%d", &choice) != 1 || choice < 1 || choice > 4){
            printf("Invalid input !!! Enter number between 1 to 4.\n");
            while((c = getchar()) != '\n' && c != EOF);
            continue;
        }

        switch (choice){
        case 1:
            printf("Enter Filename to encrypt: ");
            scanf("%s", fileName);

            strcpy(inputFileName, fileName);
            strcat(inputFileName, ".txt" );

            inputFile = fopen(inputFileName, "r");
            if (inputFile == NULL){
                perror("Error opening input file.\n");
                free(fileName);
                free(inputFileName);
                return 1;
            }
            
            encryptFile(inputFile, fileName);

            fclose(inputFile);

            break;
        
        case 2:
            printf("Enter Filename to decrypt: ");
            scanf("%s", fileName);

            strcpy(inputFileName, fileName);
            strcat(inputFileName, ".txt" );

            inputFile = fopen(inputFileName, "r");
            if (inputFile == NULL){
                perror("Error opening input file.\n");
                free(fileName);
                free(inputFileName);
                return 1;
            }

            decryptFile(inputFile, fileName);

            fclose(inputFile);
            
            break;

        case 3:

            int manageOption = 0;
            int ca;

            do
            {
                printf("File Management.\n");
                printf("1. Create file.\n");
                printf("2. Edit file.\n");
                printf("3. Delete file.\n");
                printf("4. Back to main menu.\n");
                printf("Choice (1-4): ");

                if ((scanf("%d", &manageOption)) != 1 || manageOption < 1 || manageOption > 4){
                    printf("Invalid input !!! Enter number from 1 to 4.\n");
                    while((ca = getchar()) != '\n' && ca != EOF);
                    continue;
                }

                switch (manageOption){
                case 1:
                    printf("Enter Filename to create: ");
                    scanf("%s", fileName);

                    strcpy(inputFileName, fileName);
                    strcat(inputFileName, ".txt" );

                    createFile(inputFileName);
                    break;
                
                case 2:
                    printf("Enter Filename to edit: ");
                    scanf("%s", fileName);

                    strcpy(inputFileName, fileName);
                    strcat(inputFileName, ".txt" );

                    editFile(inputFileName);
                    break;
                
                case 3:
                    printf("Enter Filename to delete: ");
                    scanf("%s", fileName);

                    strcpy(inputFileName, fileName);
                    strcat(inputFileName, ".txt" );

                    deleteFile(inputFileName);
                    break;

                case 4: 
                    printf("Exiting back to main menu");
                    break;

                default:
                    printf("Invalid input !!!");
                    break;
                }
                
            } while (manageOption != 4);
            
            break;

        case 4:
            printf("Exiting program.");
            break;

        default:
            printf("Invalid Input!!!");
            break;
        }

    } while (choice != 4);

    free(fileName);
    free(inputFileName);
    return 0;
}
